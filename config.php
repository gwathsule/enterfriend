<?php
// +------------------------------------------------------------------------+
// | @author Deen Doughouz (DoughouzForest)
// | @author_url 1: http://www.wowonder.com
// | @author_url 2: http://codecanyon.net/user/doughouzforest
// | @author_email: wowondersocial@gmail.com   
// +------------------------------------------------------------------------+
// | WoWonder - The Ultimate PHP Social Networking Platform
// | Copyright (c) 2016 WoWonder. All rights reserved.
// +------------------------------------------------------------------------+
// MySQL Hostname
$sql_db_host = "localhost";
// MySQL Database User
$sql_db_user = "root";
// MySQL Database Password
$sql_db_pass = "root";
// MySQL Database Name
$sql_db_name = "u416640193_ef";

// Site URL
$site_url = "http://localhost/vagrant/enterfriends/"; // e.g (http://example.com)

// Purchase code
$purchase_code = "1234"; // Your purchase code, don't give it to anyone. 
?>